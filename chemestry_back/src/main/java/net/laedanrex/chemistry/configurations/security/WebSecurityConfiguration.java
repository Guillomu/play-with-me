package net.laedanrex.chemistry.configurations.security;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@Order(1)
@RequiredArgsConstructor
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

	private final PasswordEncoder passwordEncoder;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
				.requestMatchers()
				.antMatchers("/oauth/**")
				.and()
				.authorizeRequests()
				//.antMatchers("/public/**").permitAll()
				.anyRequest().fullyAuthenticated()
				.and()
				.csrf().disable()
				.formLogin().disable();
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication()
				.withUser("Guilaume")
				.password(passwordEncoder.encode("grougroult"))
				.roles("USER");
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		//TODO(spring-security): not the best way ?...
		web.ignoring().antMatchers("/public/**");
	}

	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		// needed for grant_type password
		return super.authenticationManagerBean();
	}

}