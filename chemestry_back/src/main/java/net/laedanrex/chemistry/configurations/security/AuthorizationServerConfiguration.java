package net.laedanrex.chemistry.configurations.security;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;

@Configuration
@RequiredArgsConstructor
@EnableAuthorizationServer
public class AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter {

	private final PasswordEncoder passwordEncoder;
	private final AuthenticationManager authenticationManager;

	@Override
	public void configure(
			AuthorizationServerSecurityConfigurer oauthServer) throws Exception {
		oauthServer.tokenKeyAccess("permitAll()")
				.checkTokenAccess("isAuthenticated()");
	}

	@Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
		clients.inMemory()
				.withClient("clientWithGrantPassword_id")
				.secret(passwordEncoder.encode("clientWithGrantPassword_secret"))
				.authorizedGrantTypes("password", "authorization_code", "client_credentials", "implicit", "refresh_token")
				.scopes("user_info")
				.autoApprove(true)
				.redirectUris("http://localhost:8082/ui/login", "http://localhost:8083/ui2/login");
	}

	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) {
		// needed for grant_type password
		endpoints.authenticationManager(authenticationManager);
	}

}
