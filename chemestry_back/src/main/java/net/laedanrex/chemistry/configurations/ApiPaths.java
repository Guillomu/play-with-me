package net.laedanrex.chemistry.configurations;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import lombok.experimental.UtilityClass;

@UtilityClass
@FieldDefaults(level = AccessLevel.PUBLIC, makeFinal = true)
public class ApiPaths {

	public final String PUBLIC = "/public";
	public final String USER = "/user";

	public final String SEARCH = "/search";


}
