package net.laedanrex.chemistry.repositories;

import net.laedanrex.chemistry.entities.Molecule;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MoleculeRepository extends PagingAndSortingRepository<Molecule, String> {

}
