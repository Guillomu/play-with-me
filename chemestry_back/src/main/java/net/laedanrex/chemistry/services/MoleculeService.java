package net.laedanrex.chemistry.services;

import lombok.RequiredArgsConstructor;
import net.laedanrex.chemistry.repositories.MoleculeRepository;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MoleculeService {

	private final MoleculeRepository moleculeRepository;

}
