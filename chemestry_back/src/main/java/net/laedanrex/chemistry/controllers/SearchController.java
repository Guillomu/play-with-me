package net.laedanrex.chemistry.controllers;

import lombok.RequiredArgsConstructor;
import net.laedanrex.chemistry.configurations.ApiPaths;
import net.laedanrex.chemistry.controllers.dto.SearchDTO;
import net.laedanrex.chemistry.services.MoleculeService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(ApiPaths.PUBLIC)
@RequiredArgsConstructor
public class SearchController {

	private final MoleculeService moleculeService;

	@PostMapping(value = ApiPaths.SEARCH, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity searchAll(@RequestBody @Nullable SearchDTO searchDTO) {
		return ResponseEntity.ok(searchDTO);
	}

	@GetMapping(value = ApiPaths.SEARCH, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity getOne(@RequestBody @Nullable SearchDTO searchDTO) {
		return ResponseEntity.ok(searchDTO);
	}

}
