package net.laedanrex.chemistry.controllers.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;
import net.laedanrex.chemistry.entities.Atome;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AtomeDTO {

	Integer protonNumber;

	Integer neutronNumber;

	public static AtomeDTO buildWith(Atome atome) {
		return AtomeDTO.builder()
				.neutronNumber(atome.getNeutronNumber())
				.protonNumber(atome.getProtonNumber())
				.build();
	}

}
