package net.laedanrex.chemistry.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.laedanrex.chemistry.controllers.dto.SearchDTO;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Search {

	private String originalValue;
	private List<String> orderedWords;

	public static Search buildWith(SearchDTO search) {
		return Search.builder()
				.originalValue(search.getValue())
				.orderedWords(Stream.of(search.getValue().split(" ")).sorted().collect(Collectors.toList()))
				.build();
	}

}
