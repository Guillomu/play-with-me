package net.laedanrex.chemistry.entities;

import lombok.*;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.util.List;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Reaction extends BaseEntity {

	@ElementCollection
	@EqualsAndHashCode.Exclude
	List<String> labels;

	@EqualsAndHashCode.Exclude
	@ManyToMany
	List<Atome> atomes;

	@EqualsAndHashCode.Exclude
	@ManyToMany
	List<Molecule> molecules;

}
