package net.laedanrex.chemistry.entities;

import lombok.*;

import javax.persistence.Entity;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Atome extends BaseEntity {

	/**
	 * A covalent bond, also called a molecular bond, is a chemical bond that involves the sharing of electron pairs between atoms.
	 * It's the number of pairs of electrons that a given atom shares with its neighbors.
	 */
	private Integer covalentBond;

	/**
	 * The atomic number or proton number of a chemical element is the number of protons found in the nucleus of every atom of that element.
	 * (symbol Z)
	 */
	private Integer protonNumber;

	/**
	 * The neutron number is the number of neutrons in a nuclide.
	 * (symbol N)
	 */
	private Integer neutronNumber;

	/**
	 * 2 letters symbol
	 */
	private String symbol;

	/**
	 * Atomic number (proton number) plus neutron number equals mass number: Z + N = A.
	 */
	public Integer getMassNumber() {
		return protonNumber + neutronNumber;
	}

	/**
	 * The difference between the neutron number and the atomic number is known as the neutron excess: D = N - Z = A - 2Z.
	 */
	public Integer getNeutronExcess() {
		return neutronNumber - protonNumber;
	}

	/**
	 * The classic form of an atom.
	 * <p>
	 * Les isotopes sont des atomes qui possèdent le même nombre d'électrons
	 * et donc de protons, pour rester neutre -, mais un nombre différent de neutrons.
	 * On connaît actuellement environ 325 isotopes naturels et 1200 isotopes créés artificiellement.
	 */
	//private Atome mainIsotope;
}
