package net.laedanrex.chemistry.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Objects;
import java.util.UUID;

@Data
@MappedSuperclass
@NoArgsConstructor
@AllArgsConstructor
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class BaseEntity {

	@Id
	@Column(name = "id")
	private String id;

	@PrePersist
	private void generateID() {
		if (Objects.isNull(id)) {
			id = UUID.randomUUID().toString();
		}
	}

}
