package com.simplon.chemestry.business.dtos;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class AtomDto {

    @NotNull
    @Size(min = 1, max = 45)
    private String name;

    @NotNull
    @Size(min = 1, max = 45)
    private String symbol;

    private float atomicWeight;

    private float meltingPoint;

    private float boilingPoint;

    public AtomDto(){

    }

    public AtomDto(String name, String symbol, float atomicWeight){
        this.name = name;
        this.symbol=symbol;
        this.atomicWeight = atomicWeight;
    }
    public String getName() {
        return null;
    }

    public String getSymbol() {
        return null;
    }

    public String getAtomicWeight() {
        return null;
    }

    public String getMeltingPoint() {
        return null;
    }

    public String getBoilingPoint() {
        return null;
    }
}
