package com.simplon.chemestry.business.repositories;

import com.simplon.chemestry.business.dtos.AtomDto;
import com.simplon.chemestry.business.dtos.AtomViewDto;
import com.simplon.chemestry.business.entities.Atom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AtomJpaRepository extends JpaRepository<Atom, Long> {

    AtomViewDto getOneById(Long id);

    @Query("select new com.simplon.chemestry.business.dtos.AtomDto"
            + "(d.name, d.symbol, d.atomicWeight) " + "from Atom d")
    List<AtomDto> getAllProjectedBy();
}
