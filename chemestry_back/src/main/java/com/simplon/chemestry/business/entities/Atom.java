package com.simplon.chemestry.business.entities;

import javax.persistence.*;

@Entity
@Table(name="t_atom")
public class Atom extends AbstractEntity {

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String symbol;

    @Column
    private float atomicWeight;

    @Column
    private float meltingPoint;

    @Column
    private float boilingPoint;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public float getAtomicWeight() {
        return atomicWeight;
    }

    public void setAtomicWeight(float atomicWeight) {
        this.atomicWeight = atomicWeight;
    }

    public float getMeltingPoint() {
        return meltingPoint;
    }

    public void setMeltingPoint(float meltingPoint) {
        this.meltingPoint = meltingPoint;
    }

    public float getBoilingPoint() {
        return boilingPoint;
    }

    public void setBoilingPoint(float boilingPoint) {
        this.boilingPoint = boilingPoint;
    }
}
