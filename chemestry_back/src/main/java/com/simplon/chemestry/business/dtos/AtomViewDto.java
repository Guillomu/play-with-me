package com.simplon.chemestry.business.dtos;

public interface AtomViewDto {

    Long getId();

    String getName();

    String getSymbol();

    String getAtomicWeight();

    String getMeltingPoint();

    String getBoilingPoint();
}
