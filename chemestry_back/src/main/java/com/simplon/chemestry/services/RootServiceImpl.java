package com.simplon.chemestry.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Service;

@Service
public class RootServiceImpl implements RootService {

    @Autowired
    CacheManager cacheManager;

    protected RootServiceImpl(){

    }

    @Override
    public void clearCacheRegion(String region) {
        cacheManager.getCache(region).clear();
    }

    @Override
    public void clearCacheRegions() {
        for(String region : cacheManager.getCacheNames()){
            cacheManager.getCache(region).clear();
        }
    }
}
