package com.simplon.chemestry.services;

public interface RootService {

    void clearCacheRegion(String region);

    void clearCacheRegions();
}
