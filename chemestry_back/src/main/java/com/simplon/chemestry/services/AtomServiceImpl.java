package com.simplon.chemestry.services;

import com.simplon.chemestry.business.dtos.AtomDto;
import com.simplon.chemestry.business.dtos.AtomViewDto;
import com.simplon.chemestry.business.entities.Atom;
import com.simplon.chemestry.business.repositories.AtomJpaRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AtomServiceImpl implements AtomService{

    private final AtomJpaRepository repo;

    protected AtomServiceImpl(AtomJpaRepository repo){
        this.repo = repo;
    }

    @Autowired
    private ModelMapper mapper;

    @Override
    @Cacheable("atoms")
    public List<AtomDto> getAll(){
        return repo.getAllProjectedBy();
    }

    @Override
    @Cacheable("atoms")
    public AtomViewDto one(Long id){ return repo.getOneById(id); }

}
