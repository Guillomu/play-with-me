package com.simplon.chemestry.services;

import com.simplon.chemestry.business.dtos.AtomDto;
import com.simplon.chemestry.business.dtos.AtomViewDto;
import org.springframework.context.annotation.Bean;

import java.util.List;

public interface AtomService {

    List<AtomDto> getAll();

    AtomViewDto one(Long id);
}
