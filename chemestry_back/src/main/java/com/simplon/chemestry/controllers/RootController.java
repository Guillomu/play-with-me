package com.simplon.chemestry.controllers;

import com.simplon.chemestry.services.RootService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/root")
public class RootController {

    private final RootService service;

    protected RootController(RootService service){
        this.service = service;
    }

    @DeleteMapping("/caches/{region}")
    protected void clearCacheRegion(@PathVariable("region") String region){
        service.clearCacheRegion(region);
    }

    @DeleteMapping("/caches")
    protected void clearCacheRegion(){
        service.clearCacheRegions();
    }

}
