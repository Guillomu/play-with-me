package com.simplon.chemestry.controllers;

import com.simplon.chemestry.business.dtos.AtomDto;
import com.simplon.chemestry.business.dtos.AtomViewDto;
import com.simplon.chemestry.services.AtomService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/atoms")
public class AtomController {

    private final AtomService service;

    protected AtomController(AtomService service) {
        this.service = service;
    }

    @GetMapping("/{id}")
    public AtomViewDto one(@PathVariable("id") Long id) {
        return service.one(id);
    }

    @GetMapping
    public List<AtomDto> getAll() {
        return service.getAll();
    }
}
