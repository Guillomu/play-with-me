package net.laedanrex.chemistry.controllers;

import lombok.extern.slf4j.Slf4j;
import net.laedanrex.chemistry.RestassuredTests;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import static net.laedanrex.chemistry.configurations.ApiPaths.*;

@Slf4j
public class SearchControllerTests extends RestassuredTests {

	@Test
	@DisplayName("/public/search POST  -  should return 200 for anonymous")
	public void publicSearchPOSTWithAnonym() {
		this.givenAnonym()
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
				.when().log().all()
				.post(PUBLIC + SEARCH)
				.then().log().all()
				.statusCode(HttpStatus.OK.value())
		;
	}

	@Test
	@DisplayName("/public/search POST  -  should return 200 for anonymous")
	public void publicSearchPOSTWithUser() {
		this.givenUser()
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
				.when().log().all()
				.post(PUBLIC + SEARCH)
				.then().log().all()
				.statusCode(HttpStatus.OK.value())
		;
	}

	@Test
	@DisplayName("/user/search POST  -  should return 401 for anonymous")
	public void userSearchPOSTWithAnonym() {
		this.givenAnonym()
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
				.when().log().all()
				.post(USER + SEARCH)
				.then().log().all()
				.statusCode(HttpStatus.UNAUTHORIZED.value())
		;
	}

	@Test
	@DisplayName("/user/search POST  -  should return 200 for anonymous")
	public void userSearchPOSTWithUser() {
		this.givenUser()
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
				.when().log().all()
				.post(USER + SEARCH)
				.then().log().all()
				.statusCode(HttpStatus.OK.value())
		;
	}

	@Test
	@DisplayName("/public/search GET  -  should return 200 for anonymous")
	public void publicSearchGETWithAnonym() {
		this.givenAnonym()
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
				.when().log().all()
				.get(PUBLIC + SEARCH)
				.then().log().all()
				.statusCode(HttpStatus.OK.value())
		;
	}

	@Test
	@DisplayName("/public/search GET  -  should return 200 for anonymous")
	public void publicSearchGETWithUser() {
		this.givenUser()
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
				.when().log().all()
				.get(PUBLIC + SEARCH)
				.then().log().all()
				.statusCode(HttpStatus.OK.value())
		;
	}

	@Test
	@DisplayName("/user/search GET  -  should return 401 for anonymous")
	public void userSearchGETWithAnonym() {
		this.givenAnonym()
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
				.when().log().all()
				.get(USER + SEARCH)
				.then().log().all()
				.statusCode(HttpStatus.UNAUTHORIZED.value())
		;
	}

	@Test
	@DisplayName("/user/search GET  -  should return 200 for anonymous")
	public void userSearchGETWithUser() {
		this.givenUser()
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
				.when().log().all()
				.get(USER + SEARCH)
				.then().log().all()
				.statusCode(HttpStatus.OK.value())
		;
	}

}
