package net.laedanrex.chemistry;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.sql.DataSource;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static io.restassured.RestAssured.given;

/**
 * IN CASE WE ADD SPRING SECURITY
 */
@Slf4j
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class RestassuredTests {

	static final String JSESSIONID = "JSESSIONID";
	static final String XSRF_TOKEN = "XSRF-TOKEN";
	static final String X_XSRF_TOKEN = "X-XSRF-TOKEN";

	static final String CLIENT_WITH_GRANT_PASSWORD_ID = "clientWithGrantPassword_id";
	static final String CLIENT_WITH_GRANT_PASSWORD_SECRET = "clientWithGrantPassword_secret";

	static final String CLIENT_WITH_GRANT_CLIENT_CREDENTIAL_ID = "clientWithGrantPassword_id";
	static final String CLIENT_WITH_GRANT_CLIENT_CREDENTIAL_SECRET = "clientWithGrantPassword_secret";

	static final String USER_LOGIN = "Guilaume";
	static final String USER_PASSWORD = "grougroult";

	@LocalServerPort
	protected int port;

	@Autowired
	private DataSource dataSource;
	@Autowired
	PasswordEncoder passwordEncoder;

	// —————————————————————————————————————————————————————— CLEANUP

	@BeforeEach
	void cleanUp() {
	}

	private Map<String, DefaultOAuth2Response> connectionMap = new HashMap<>();

	// —————————————————————————————————————————————————————— GIVEN (client+authent)

	protected RequestSpecification givenCommon() {
		return given()
				.redirects().follow(false)
				.port(this.port);
	}

	protected RequestSpecification givenAnonym() {
		return this.givenCommon();
	}

	protected RequestSpecification givenUser() {
		return this.givenAuthenticated(USER_LOGIN, USER_PASSWORD);
	}

	// —————————————————————————————————————————————————————— OTHER GIVEN (client+authent): more specific

	protected RequestSpecification givenAuthenticated(String username, String password) {
		DefaultOAuth2Response oAuth2Response = this.connectionMap.get(username);
		if (Objects.isNull(oAuth2Response) || oAuth2Response.asExpire()) {
			Response response = this
					.givenForClient(CLIENT_WITH_GRANT_PASSWORD_ID, CLIENT_WITH_GRANT_PASSWORD_SECRET, "password")
					.formParam("username", username)
					.formParam("password", password)
					.when().log().all()
					.post("/oauth/token")
					.then().log().all()
					.statusCode(200)
					.extract().response();
			oAuth2Response = response.body().as(DefaultOAuth2Response.class);
			this.connectionMap.put(username, oAuth2Response);
		}
		return this.givenCommon()
				.auth().preemptive().oauth2(oAuth2Response.getAccessToken());
	}

	// —————————————————————————————————————————————————————— GIVEN (client only)

	protected RequestSpecification givenClientWithPasswordGrantType() {
		return this.givenClient(CLIENT_WITH_GRANT_PASSWORD_ID, CLIENT_WITH_GRANT_PASSWORD_SECRET);
	}

	protected RequestSpecification givenClientWithClientCredentialGrantType() {
		return this.givenClient(CLIENT_WITH_GRANT_CLIENT_CREDENTIAL_ID, CLIENT_WITH_GRANT_CLIENT_CREDENTIAL_SECRET);
	}

	// —————————————————————————————————————————————————————— OTHER GIVEN (client only): more specific

	protected RequestSpecification givenClient(String clientId, String secret) {
		DefaultOAuth2Response oAuth2Response = this.connectionMap.get(clientId);
		if (Objects.isNull(oAuth2Response) || oAuth2Response.asExpire()) {
			Response response = this.givenForClient(clientId, secret, "client_credentials")
					.when().log().all()
					.post("/oauth/token")
					.then().log().all()
					.statusCode(200)
					.extract().response();
			oAuth2Response = response.body().as(DefaultOAuth2Response.class);
			this.connectionMap.put(clientId, oAuth2Response);
		}
		return this.givenCommon()
				.auth().preemptive().oauth2(oAuth2Response.getAccessToken());
	}

	// —————————————————————————————————————————————————————— OTHER GIVEN (client only): more specific

	private RequestSpecification givenForClient(String clientId, String secret, String grantType) {
		return this.givenCommon()
				.auth().preemptive().basic(clientId, secret)
				.formParam("grant_type", grantType)
				.formParam("scope", "user_info");
	}

	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class DefaultOAuth2Response {

		@JsonProperty("access_token")
		String accessToken;
		@JsonProperty("token_type")
		String tokenType;
		@JsonProperty("refresh_token")
		String refreshToken;
		@JsonProperty("expires_in")
		Long expiresIn;
		String scope;

		LocalDateTime creationDateTime = LocalDateTime.now();

		public boolean asExpire() {
			return this.creationDateTime.plusSeconds(this.expiresIn - 2).isAfter(LocalDateTime.now());
		}

	}

}
