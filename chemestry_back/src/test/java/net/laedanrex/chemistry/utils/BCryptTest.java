package net.laedanrex.chemistry.utils;

import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class BCryptTest {

	@Test
	public void printEncodedPassword() {
		System.out.println(new BCryptPasswordEncoder().encode("my-password"));
	}

}
