truncate table users;
truncate table authorities;
truncate table oauth_client_details;
truncate table oauth_client_token;
truncate table oauth_access_token;
truncate table oauth_refresh_token;
truncate table oauth_code;
truncate table oauth_approvals;

-- uses net.laedanrex.chemistry.utils.BCryptTest to encode password
insert into users (username, password)
values ('guillaume', 'demande-id-1'),
       ('alexandre', 'demande-id-1')
;

insert into authorities (username, authority)
values ('guillaume', 'PETIT_SCOPE'),
       ('alexandre', 'BON_GROS_SCOPE')
;

-- uses net.laedanrex.chemistry.utils.BCryptTest to encode password
insert into oauth_client_details (client_id, resource_ids, client_secret, scope, authorized_grant_types,
                                  web_server_redirect_uri, authorities, access_token_validity, refresh_token_validity,
                                  additional_information, autoapprove)
values ('guillaume', 'PETIT_SCOPE', 'true'),
       ('alexandre', 'BON_GROS_SCOPE')
;

