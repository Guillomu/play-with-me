# ARCHITECTURE

Toutes les API nécessite une authentification.   
Sauf API publique : `/public/**` 


# IDEE ET DIRECTION

utiliser une DB NoSQL pour la puissance de rapidité de recherche de texte ?   
(Elasticsearch > MongoDB)

### IDEE POUR MODELE

categorie atome
- liaison covalente
- caractéristiques (nb protons/nucleons/electrons)
- statistiques? // pourcentage de presence // stabilité
- sous categorie (difference de caractéristiques)
- labels (radioactif/noble/...) -> endpoints sur les labels
- etatsFrequents (H2O-eau ...)

categorie molecule (association d'atomes)
- atomes
- labels // commonNames [nitro, essence, ...]
- reactions (resultat(de laquelle elle resulte)/producteur(dans laquelle elle intervient)) 

categorie reaction
- labels (couleur/explosion/inflamable//niveau de chaque reactions)
- elements impliqué
- conditions (temperature/humidité...)

peut etre pas besoin de categorie:
- chaque éléments est constitué d'un ou plusieurs éléments
- chaque element constitue un ou plusieurs éléments
- mais peut etre necessaire car les réaction entre atomes dependent 
 peut-etre de leur constitution (carbone 12, carbone 14) 

### MILSTONE LOINTAIN

utiliser graphql pour la suite logique de resultat:
- on enregistre le chemin de tous les utilisateurs
- chaque clic amene sur une page -> les noeuds
- precedent revient sur le noeud precedent
- proposer une direction selon l'aboutissement de la plupart des graph ? 
 // un mot clef auquel aboutit ces recherches

# CHARTE GRAPHIQUE

logo colors :
- bleu marine #213B70
- bleu ciel #01A4E6 





